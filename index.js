let http = require('http')

let port = 4000 

const server = http.createServer(function(request, response){
	// request.method is used to get the HTTP method that is currently used by the browser to send a request
	if(request.url == '/items' && request.method == 'GET'){ //in this case, we are checking if the current request method is a GET HTTP method
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data retrieved from the database')
	}

	if(request.url == '/items' && request.method == 'POST'){ //In this one, we are checking if the current request method is a POST HTTP method
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data sent to the database')
	}
	
})

server.listen(port)
console.log(`Server is running at localhost: ${port}`)